var file;
var sharedURL;
var imageToSend;
//var token = "YOU REALLY THINK I WILL PUT IT HERE LOL";
window.addEventListener("load", pageFullyLoaded, false);

function pageFullyLoaded() {
    getCurrentDateTime();
    document.getElementById("fileUpload").addEventListener("change", function() {
        file = this.files[0];
        console.log(file);
        document.getElementById("upload").addEventListener("click", upload);
    });
    document.getElementById("createURL").addEventListener("click", createURL);
    document.getElementById("sendReq").addEventListener("click", sendReq);
}

// Usage: click "Choose File", then click "SendReq", then go to backend to check what's in the req.
function sendReq() {
    var postUrl = "https://webdevbootcamp-shalltalk.c9users.io/quotes/plugin/";
    // var username = document.getElementById("username");
    // var password = document.getElementById("password");
    var username = "a@a.com";
    var password = "password";
    var reader = new FileReader();
    reader.onloadend = function() {
        imageToSend = reader.result;
        console.log("done");
        var formData = {
            // info like product name and price will be processed at backend based on webUrl
            // imageUrl will be created in the backend after uploading the imgae (file here) to dropbox.
            "username": username,
            "password": password,
            "image" : imageToSend,
            "webUrl" : "https://www.amazon.com/Acer-Chromebook-Aluminum-Quad-Core-CB3-431-C5FM/dp/B01CVOLVPA/",
            "description" : "initial request test."
        };
        var request = new XMLHttpRequest();
        request.onload = function() {
            if (request.status === 200) {
                alert("status is 200");
            } else {
                var errorMessage = request.response || "Unable to upload file";
                alert(errorMessage);
            }
        };
        request.open("POST", postUrl, true);
        request.setRequestHeader("Content-Type", "application/json");
        alert(JSON.stringify(formData));
        request.send(JSON.stringify(formData));
    }
    reader.readAsDataURL(file);
}

function upload() {
    var xhr = new XMLHttpRequest();
    xhr.upload.onprogress = function(evt) {
        var percentComplete = parseInt(100.0 * evt.loaded / evt.total);
        // Upload in progress. Do something here with the percent complete.
    };

    xhr.onload = function() {
        if (xhr.status === 200) {
            var fileInfo = JSON.parse(xhr.response);
            // Upload succeeded. Do something here with the file info.
            alert("status is 200");
        } else {
            var errorMessage = xhr.response || "Unable to upload file";
            // Upload failed. Do something here with the error.
            alert(errorMessage);
        }
    };

    xhr.open("POST", "https://content.dropboxapi.com/2/files/upload");
    xhr.setRequestHeader("Authorization", "Bearer " + token);
    xhr.setRequestHeader("Content-Type", "application/octet-stream");
    xhr.setRequestHeader("Dropbox-API-Arg", JSON.stringify({
        // "username" here can be the username, if it does not exist, a new folder will be created in dropbox.
        path: "/" + "username" + "/" + file.name,
        mode: "add",
        autorename: true,
        mute: true
    }));

    xhr.send(file);
}

// filePath should be passed into, the filePath should be generated in the upload method based on username.
function createURL() {

    var filePath = "/username/IMG_20170505_205932.jpg";
    var data = {
        path: filePath,
    }

    xhr = new XMLHttpRequest();
    var url = "https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Authorization", "Bearer " + token);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                var json = JSON.parse(xhr.responseText);
                sharedURL = replaceTrailingZero(json.url);
                console.log("sharedURL is : " + sharedURL);
            } else {
                console.log("error in createURL");
                console.log(xhr.responseText);
            }
        }
    }
    xhr.onloadend = function() {
        if (xhr.status == 409) {
            console.log("shared url already exist for " + filePath + ", trying to get existing shared url...");
            getExistingURL(filePath);
        }
    }
    data = JSON.stringify(data);
    xhr.send(data);
}

function getExistingURL(filePath) {
    console.log("getExistingURL WITH PARAM called");
    var data = {
        path: filePath,
    }

    xhr = new XMLHttpRequest();
    var url = "https://api.dropboxapi.com/2/sharing/list_shared_links";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Authorization", "Bearer " + token);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                var json = JSON.parse(xhr.responseText);
                sharedURL = replaceTrailingZero(json.links[0].url);
                console.log("sharedURL is : " + sharedURL);
            } else {
                console.log("error in getExistingURL");
                console.log(xhr.responseText);
            }
        }
    }
    data = JSON.stringify(data);
    xhr.send(data);
}

function replaceTrailingZero(url) {
    var newURL = url.substring(0, url.length - 1);
    return newURL + "1";
}
function getCurrentDateTime() {
    var datetime = new Date();
    console.log(datetime);
}
window.addEventListener("load", function() {
    var loginButton = document.getElementById("login");
    loginButton.addEventListener("click", login, false);

    chrome.tabs.query({
        currentWindow: true,
        active: true
    }, function(tabs) {
        var url = tabs[0].url;
        console.log(url);
        chrome.storage.sync.get(["username", "password"], function(items) {
            var username = items.username;
            var password = items.password;
            if(typeof username === "undefined" || typeof password === "undefined") {
                alert("Please log in first, then click the icon again");
                return;
            }
            console.log(username);
            console.log(password);
            //TODO: make request to post quote
            //TODO: add the url to post to permission of manifest

        });
    });
});

function login(signupForm) {
    var registerUrl = "https://webdevbootcamp-shalltalk.c9users.io/loginFromPlugin"; // TODO: systme variable
    var username = document.getElementById("username");
    var password = document.getElementById("password");
    if (username.value == null || username.value == "") {
        alert("Please input username");
        return;
    }
    if (password.value == null || password.value == "") {
        alert("Please input password");
        return;
    }
    var formData = {
        "username": username.value,
        "password": password.value
    };
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (request.readyState == XMLHttpRequest.DONE) {
            alert(request.responseType + "-" + request.status + "-" + request.statusText + "-" + request.response);
            JSON.stringify(request.response);
            if (request.response === "authentication failed"){
                alert("Authentication failed, please log in again.");
                return;
            }
            chrome.storage.sync.set(formData, function() {
                console.log("saved");
            });
        }
    }
    request.open("POST", registerUrl, true);
    request.setRequestHeader("Content-Type", "application/json");
    alert(JSON.stringify(formData));
    request.send(JSON.stringify(formData));
}